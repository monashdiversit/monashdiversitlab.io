---
title: "About"
menu: "main"
---
# About
There is a huge lack of diversity and representation in the IT industry. Despite there being a vast amount of research that praises the benefits of having a diverse workforce - more efficient, and creative teams, not to mention having a diverse team means better representing most audiences as well, we still have an outrageous gender imbalance, people who still feeling marginalised in the industry and a stereotype of that somebody in tech is one type of person.

We want those things to change.

diversIT aims to make an impact on underrepresented students within IT, by connecting them with industry, faculty and each other. We want to develop their confidence and leadership skills by providing resources they would not have access to otherwise, and encouraging more students to enter the IT field.

