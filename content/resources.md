---
title: "Resources"
menu: "main"
---
# Resources
## Our Newsletter
+ [**diversIT newsletter**](https://us15.campaign-archive.com/home/?u=1fe8aa94259585cfc46125cfe&id=a53ae876a4) posts upcoming diversity-related events, job opportunities and general updates once a week. If you're interested, please sign up!

## Academic
### Student Packages
+ [**Free Microsoft Office 365**](https://www.monash.edu/esolutions/software/microsoft-365)
Monash provides students access to download Microsoft Office 365 on up to five devices for free.  
You can download Word, Excel, PowerPoint, OneNote, Publisher and Access for free.

+ [**Github Student Developer Pack**](https://education.github.com/pack)
Get unlimited private repositories for Github (normally $7 a month)

+ [**Jetbrains Professional Developer Tools**](https://www.jetbrains.com/student/)
Jetbrains produces IDEs (a software program that helps you code) for a variety of languages.

### Learn Coding 
+ [**The Odin Project**](https://www.theodinproject.com/)
is a free, open source and well maintained curriculum for full stack development.

+ [**Codecademy**](https://www.codecademy.com/)
is a website that offers free coding classes in 12 different programming languages including Python, Java, PHP, JavaScript, Ruby, SQL and Sass, as well as HTML and CSS.

+ [**Grok Learning**](https://groklearning.com/)
also offers online coding classes. They charge $20 for a single course.

+ [**W3Cschools.com**](https://www.w3schools.com/)
offers free tutorials for web development. They have HTML, CSS, JavaScript, SQL, PHP, jQuery, Bootstrap and even website templates.

+ [**General Assembly**](https://www.facebook.com/gnrlassembly/events/)
is holding a bunch of free intro workshops on a variety of topics including UX Design, Coding, Data Analytics and many more.

+ [**CodeWars**](https://www.codewars.com/)
Do you enjoy learning through games? Through a martial arts theme, CodeWars teaches you to code, awarding you as you complete challenges (called kata). There are a variety of languages available, including Python, Java, PHP, Ruby, C++ and more.  

+ [**Python Guide**](https://cs.stanford.edu/people/nick/py/)
Nick Parlante's official Python guide for use with CS106A (Programming Methodology) at Stanford University.

### Cyber Security
Interested in Cyber Security? Try the [picoCTF](https://play.picoctf.org/practice) challenge.
Solve a series of problems designed for high school students including forensics, cryptography, reverse engineering, web exploitation, binary exploitation.

<!---
### UX
### UI
-->
## Social
### Women
+ [**Code Like a Girl**](https://www.codelikeagirl.com/)
Code Like a Girl is an Australian organisation that aims to inspire more females to pursue careers in coding and get involved in the creation and development of technology.

+ [**VicICT4Women**](http://www.vicictforwomen.com.au/)
Women in tech are a smart lot, working as developers, analysts, programmers and everything in between.  
However, while some can code in a number of complex languages and have all the smarts in the world, many struggle with how to progress into leadership positions. And, quite frankly, this does not compute. Vic ICT for Women is the professional organisation that is putting a rocket up the industry and creating epic pathways for women in tech leadership.

### LGBTQIA
+ [**Minus18**](https://minus18.org.au/)
is Australia’s largest youth led organisation for gay, lesbian, bisexual and trans youth. They provide support and mentoring, as well as run a bunch of social events throughout the year.

+ [**The Australian GLBTIQ Multicultural Council Directory**](http://www.agmc.org.au/resources-support/)
has a bunch of links to different cultural GLBTIQ groups in Australia.  

+ [**Rainbow Network**](http://rainbownetwork.com.au/events)
On the rainbow network, you can find a number of different youth support groups for SSAITGD (Same Sex Attracted, Trans and Gender Diverse) youths all across Victoria   

+ [**GLEAM**](http://www.monashclubs.org/Clubs/GLEAM)
is a student group for Queer+ identifying science, technology, engineering and maths students at Monash University.

### Mental Health
+ [**headspace**](https://www.headspace.org.au/)
is the National Youth Mental Health Foundation providing early intervention mental health services to 12-25 year olds, along with assistance in promoting young peoples’ wellbeing. This covers four core areas: mental health, physical health, work and study support and alcohol and other drug services.

<!---
### Religious/Cultural Groups
-->

## Career
### LGBT
+ [**Pride in Diversity**](https://www.prideinclusionprograms.com.au/) is the national not-for-profit employer support program for LGBTI workplace inclusion specialising in HR, organisational change and workplace diversity. Pride in Diversity publishes the Australian Workplace Equality Index (AWEI), Australia’s national benchmarking instrument for LGBTI workplace inclusion from which Top Employers for LGBTI people is determined.

### Girls
+ [**Codelikeagirl**](https://www.codelikeagirl.com/jobs-board/) job newsletter sends regular emails with job offers for women in IT

### Physical Disabilities
+ [**Stepping Into**](https://australiandisabilitynetwork.org.au/students-jobseekers/start-a-stepping-into-internship/) Graduate research tells us that tertiary students with disability find it more difficult to secure employment opportunities after graduation. Stepping Into program is a paid internship scheme that matches talented university students with disability with roles in leading Australian businesses.
