const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

gulp.task('sass', () => {
  gulp.src('src/sass/styles.scss')
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }),
      cssnano()
    ]))
    .pipe(gulp.dest('static/css'));
});

gulp.task('js', () => {
  gulp.src('src/js/**/*.js')
    .pipe(uglify())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('static/js'));
});

//Watch task
gulp.task('default', () => {
  gulp.start('sass');
  gulp.watch('src/sass/**/*.scss', ['sass']);
  gulp.start('js');
  gulp.watch('src/js/**/*.js', ['js']);
});
